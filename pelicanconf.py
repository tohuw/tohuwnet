"""
Pelican Configuration
This includes all documented settings as of Pelican 4.8.0
"""


## Metadata
SITENAME = 'Tohuw.Net'
SITEURL = 'https://tohuw.net'
AUTHOR = 'Ron Scott-Adams'
DEFAULT_METADATA = {'Status': 'draft',}
USE_FOLDER_AS_CATEGORY = True
DEFAULT_CATEGORY = 'miscellany'
FILENAME_METADATA = r'(?P<date>\d{4}-\d{2}-\d{2})_(?P<slug>.*)'
PATH_METADATA = ''
EXTRA_PATH_METADATA = {'extras/robots.txt': {'path': 'robots.txt'}}


## Serving
#PORT = 8000
#BIND = ''


## Paths & URLs
PATH = 'content'
OUTPUT_PATH = 'output/'
OUTPUT_SOURCES = False
OUTPUT_SOURCES_EXTENSION = '.txt'
DELETE_OUTPUT_DIRECTORY = False
OUTPUT_RETENTION = []
WRITE_SELECTED = []

INDEX_SAVE_AS = 'index.html'

STATIC_PATHS =  ['images', 'extras/robots.txt']
STATIC_EXCLUDES = []
STATIC_EXCLUDE_SOURCES = True
STATIC_CREATE_LINKS = True
STATIC_CHECK_IF_MODIFIED = False

PAGE_PATHS = ['pages']
PAGE_EXCLUDES = []
PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}.html'
PAGE_LANG_URL = '{slug}-{lang}'
PAGE_LANG_SAVE_AS = '{slug}-{lang}.html'
PAGE_ORDER_BY = 'basename'

DRAFT_PAGE_URL = 'drafts/pages/{slug}'
DRAFT_PAGE_SAVE_AS = 'drafts/pages/{slug}.html'
DRAFT_PAGE_LANG_URL = 'drafts/pages/{slug}-{lang}'
DRAFT_PAGE_LANG_SAVE_AS = 'drafts/pages/{slug}-{lang}.html'

ARTICLE_PATHS = ['articles']
ARTICLE_EXCLUDES = []
ARTICLE_URL = 'articles/{slug}.html'
ARTICLE_SAVE_AS = 'articles/{slug}.html'
ARTICLE_LANG_URL = 'articles/{slug}-{lang}'
ARTICLE_LANG_SAVE_AS = 'articles/{slug}-{lang}.html'
ARTICLE_ORDER_BY = 'reversed-date'

DRAFT_URL = 'drafts/{slug}'
DRAFT_SAVE_AS = 'drafts/{slug}.html'
DRAFT_LANG_URL = 'drafts/{slug}-{lang}'
DRAFT_LANG_SAVE_AS = 'drafts/{slug}-{lang}.html'

SLUGIFY_SOURCE = 'basename'
#SLUGIFY_USE_UNICODE = False
#SLUGIFY_PRESERVE_CASE = False
SLUG_REGEX_SUBSTITUTIONS = [(r'[^\w\s-]', ''), # remove non-alphabetical/whitespace/'-' chars
                            (r'(?u)\A\s*', ''), # strip leading whitespace
                            (r'(?u)\s*\Z', ''), # strip trailing whitespace
                            (r'[-\s]+', '-'), # reduce multiple whitespace or '-' to single '-'
                            (' and ', ''),]

AUTHORS_SAVE_AS = ''
AUTHOR_URL = '/author/{slug}'
AUTHOR_SAVE_AS = ''
AUTHOR_REGEX_SUBSTITUTIONS = [(r'[^\w\s]', ''), # remove non-alphabetical/whitespace chars
                             (r'(?u)\A\s*', ''), # strip leading whitespace
                             (r'(?u)\s*\Z', ''), # strip trailing whitespace
                             (r'[-\s]+', '-'),] # reduce multiple whitespace or '-' to single '-'

CATEGORY_URL = '/topic/{slug}'
CATEGORY_SAVE_AS = 'topic/{slug}.html'
CATEGORIES_SAVE_AS = 'topics.html'
REVERSE_CATEGORY_ORDER = False
CATEGORY_REGEX_SUBSTITUTIONS = SLUG_REGEX_SUBSTITUTIONS

#TAGS_SAVE_AS =  'tags.html'
TAG_URL = 'tag/{slug}'
#TAG_SAVE_AS = 'tag/{slug}.html'
TAG_REGEX_SUBSTITUTIONS = SLUG_REGEX_SUBSTITUTIONS

ARCHIVES_SAVE_AS = 'archives/index.html'
YEAR_ARCHIVE_URL = ''
YEAR_ARCHIVE_SAVE_AS = 'archives/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'archives/{date:%Y}/{date:%b}/index.html'
#DAY_ARCHIVE_SAVE_AS = ''
#NEWEST_FIRST_ARCHIVES = True

RELATIVE_URLS = True
IGNORE_FILES =  ['.#*', '_*']
#LOG_FILTER = []


## Pagination
#DEFAULT_ORPHANS = 0
DEFAULT_PAGINATION = True
#PAGINATED_TEMPLATES = {'index': None, 'tag': None, 'category': None, 'author': None}
PAGINATION_PATTERNS = ((1, '{url}', '{save_as}'),
                       (2, '{base_name}/page/{number}/', '{base_name}/page/{number}/index.html'),)



## Localization
DEFAULT_LANG = 'en'

import os
if os.name == 'nt':
    LOCALE = ('usa')
else:
    LOCALE = ('en_US.UTF-8')

TIMEZONE = 'America/New_York'

DEFAULT_DATE = 'fs'
DEFAULT_DATE_FORMAT = '%B %d, %Y'
#DATE_FORMATS = {
#    'en': '%B %d, %Y',
#    'jp': '%Y-%m-%d(%a)',
#}


## Translations
#ARTICLE_TRANSLATION_ID = 'slug'
#PAGE_TRANSLATION_ID = 'slug'
#TRANSLATION_FEED_ATOM = 'feeds/all-{lang}.atom.xml'
#TRANSLATION_FEED_ATOM_URL = None
#TRANSLATION_FEED_RSS = None
#TRANSLATION_FEED_RSS_URL = None


## Feeds
FEED_DOMAIN = SITEURL
# FEED_MAX_ITEMS

FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ATOM = None
CATEGORY_FEED_ATOM = None
TAG_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

FEED_ALL_RSS = None
FEED_RSS = None
CATEGORY_FEED_RSS = None
TAG_FEED_RSS = None
AUTHOR_FEED_RSS = None
TRANSLATION_FEED_RSS = None
# RSS_FEED_SUMMARY_ONLY = True


## Rendering
JINJA_ENVIRONMENT = {'trim_blocks': True,
                     'lstrip_blocks': True,
                     'extensions': ['jinja2.ext.loopcontrols',],}
JINJA_FILTERS = {}
JINJA_GLOBALS = {}
JINJA_TESTS = {}
READERS = {}
DOCUTILS_SETTINGS = {}
PYGMENTS_RST_OPTIONS = []
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {
            # 'css_class': 'highlight',
        },
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.admonition': {},
        'markdown.extensions.footnotes': {
            # 'PLACE_MARKER': '///Footnotes///',
            # 'UNIQUE_IDS': False,
            # 'BACKLINK_TEXT': '&#8617;',
        },
        'markdown.extensions.toc': {
            # 'marker': '[TOC]',
            'title': 'Contents',
            'anchorlink': True,
            # 'permalink': False,
            'baselevel': 2,
            # 'slugify' : 'markdown.extensions.headerid.slugify'
            # separator : '-'
        },
    },
    'output_format': 'html5',
}
TYPOGRIFY =  True
TYPOGRIFY_IGNORE_TAGS = []
TYPOGRIFY_DASHES = 'oldschool'
SUMMARY_MAX_LENGTH = 50
SUMMARY_END_SUFFIX = '…'
WITH_FUTURE_DATES = False
INTRASITE_LINK_REGEX = '[{|](?P<what>.*?)[|}]'
DEFAULT_PAGINATION = 10
FORMATTED_FIELDS = ['summary']

# TEMPLATE_PAGES = None
DIRECT_TEMPLATES = [
#    'about',
    'archives',
#    'articles',
    'authors',
    'categories',
    'index',
#    'search',
    'tags',
]


## Caching
CACHE_CONTENT = False
CONTENT_CACHING_LAYER = 'reader'
CACHE_PATH = 'cache'
GZIP_CACHE = True
CHECK_MODIFIED_METHOD = 'mtime'
LOAD_CONTENT_CACHE = False


## Plugins
PLUGINS = None
PLUGIN_PATHS = []


## Themes
THEME = 'themes/flex'
#THEME_STATIC_DIR = 'theme'
#THEME_STATIC_PATHS = ['static']
#THEME_TEMPLATES_OVERRIDES = []
#CSS_FILE = 'main.css'

DISPLAY_CATEGORIES_ON_MENU = False
#DISPLAY_PAGES_ON_MENU = True

## Theme-Specific Settings
SITESUBTITLE = "Clouds, Chaos, and Yak Shaving"
#MENUITEMS = ''

#GOOGLE_ANALYTICS = ''
#GA_COOKIE_DOMAIN = ''

#DISQUS_SITENAME = ''
#GOSQUARED_SITENAME = ''
#TWITTER_USERNAME = ''
#GITHUB_URL = ''
#SOCIAL_WIDGET_NAME = ''
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

#LINKS_WIDGET_NAME = ''
#LINKS = (('Pelican', 'https://getpelican.com/'),
#         ('Python.org', 'https://www.python.org/'),
#         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
#         ('You can modify those links in your config file', '#'),)
